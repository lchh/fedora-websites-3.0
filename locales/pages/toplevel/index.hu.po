#. extracted from content/pages/toplevel/index.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-20 20:55+0000\n"
"PO-Revision-Date: 2022-09-21 10:35+0000\n"
"Last-Translator: Hoppár Zoltán <hopparz@gmail.com>\n"
"Language-Team: Hungarian <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/toplevel/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Translate Toolkit 3.6.2\n"

#: title
msgid "Top Level Fedora Page"
msgstr "Kezdő része a Fedora oldalnak"

#: header-%3Etitle
msgid "It's your OS"
msgstr "Ez a Te operációs rendszered"

#: header-%3Esubtitle
msgid ""
"An innovative platform for hardware, clouds, and containers, built with love "
"by you."
msgstr ""
"Egy innovatív platform hardverekhez, felhőkhöz és konténerekhez, amelyet "
"szeretettel együtt készítettünk veled is."
